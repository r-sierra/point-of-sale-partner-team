# -*- coding: utf-8 -*-

from odoo import models


class PosOrder(models.Model):
    _inherit = "pos.order"

    def _prepare_invoice(self):
        vals = super(PosOrder, self)._prepare_invoice()
        if self.partner_id.team_id:
            vals['team_id'] = self.partner_id.team_id.id
        return vals
