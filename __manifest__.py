# -*- coding: utf-8 -*-
{
    'name': "POS - Equipo de Venta del contacto en factura",
    'summary': "Al facturar pega el Equipo de Venta del contacto"
    'description': """
        Por defecto el Equipo de Venta de la factura se obtiene del usuario que
        la realiza, este modulo altera ese funcionamiento priorizando, en caso
        que tenga, el Equipo de venta del contacto, sino utiliza el del usuario.
    """,
    'author': "Roberto Sierra <roberto@ideadigital.com.ar>",
    'website': "https://gitlab.com/r-sierra/point-of-sale-partner-team",
    'license': 'LGPL-3',
    'category': 'Sales/Point Of Sale',
    'version': '12.0.0.1.0',
    'depends': [
        'sale',
        'point_of_sale',
    ],
}
