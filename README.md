POS - Equipo de Venta del contacto en factura
---------------------------------------------

Por defecto el Equipo de Venta de la factura se obtiene del usuario que
la realiza, este modulo altera ese funcionamiento priorizando, en caso
lo que tenga definido, el Equipo de venta del contacto, sino utiliza el del usuario.

```mermaid
graph TD;
    A[Pedido] -->|Generar Factura| B{"Cliente ¿tiene Equipo de Venta?"};
    B -->|Si| C["Factura[Equipo de Venta] = Cliente[Equipo de Venta]"];
    B -->|No| D[Funcionamiento por defecto];
```
